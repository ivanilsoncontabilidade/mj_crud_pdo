<?php
    // Instancia o 'config.php'
    require_once("config.php");
    // Classe DB
    class DB{
        // atributos
        public static $db = null;
        public static $pdo;        
        // métodos
        final private function __construct(){
            try{
                self::getDb();
            } catch (Exception $e){
                $e->getMessage();
            }
        }
        public static function getInstance(){
            if(self::$db==null){
                self::$db = new self();
            }
            return self::$db;
        }
        public function getDb(){
            if(self::$pdo==null){
                self::$pdo = new PDO("mysqli:dbname=".DATABASE.";host=".HOSTNAME, USERNAME, PASSWORD,
                        array(PDO::MYSQL_ATTR_INIT_COMMAND=>"SET NAME utf8"));
            }            
            return self::$pdo;
        }
    }

